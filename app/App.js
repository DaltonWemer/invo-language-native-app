import React from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity, 
  ScrollView,
  Dimensions,
  AsyncStorage
} from 'react-native'

// Screens for the Navigator
import { Home } from './screens/home'
// Third Party Packages
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'
import { fadeIn } from 'react-navigation-transitions'
import * as Font from 'expo-font'

// Components 
export default class App extends React.Component {
  state = {
    ready: true,
    initRoute: 'Home',
    fontLoaded: false
    }


    async componentDidMount() {
      // Load in Custom Font
      Font.loadAsync({
        'Roboto': require('../assets/fonts/Roboto-Regular.ttf'),
        'RobotoBold': require('../assets/fonts/Roboto-Bold.ttf'),
      });
      this.setState({ fontLoaded: true });
    }

  render() {
    Nav = (initRoute) => {
      return createStackNavigator({
        Home: Home
      },
      {
        headerMode: 'none',
        initialRouteName: this.state.initRoute,
        transitionConfig: () => fadeIn(),
      }
    )
  }
      const AppNav = Nav(this.state.initRoute)
      const App = createAppContainer(AppNav);

      return(
          this.state.fontLoaded ? <App/> : (<View><Text>Hello</Text></View>)
        ) 
    }

}

