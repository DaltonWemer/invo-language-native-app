import React from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity, 
  ScrollView,
  Dimensions,
  Image,
  TextInput
} from 'react-native'

// Class Wide Variables
const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

// Third Party
import { Ionicons } from '@expo/vector-icons';

export default class LanguageItem extends React.Component {
  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this.props.onPress}
      >
        <Text
          style={styles.title}
        >
          {this.props.name}
        </Text>
        { this.props.selected &&
            <Ionicons
                name='md-checkmark-circle'
                color={'#77dd77'}
                size={20}
                style={styles.checkMark}
            />
        }
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   width: '100%',
   flexDirection: 'row',
   alignItems: 'flex-start',
   justifyContent: 'space-between',
   paddingVertical: 10,
   marginTop: 10,
   shadowOpacity: 0.3,
   borderRadius: 5,
   backgroundColor: '#FFF',
   shadowOffset: {
	width: 0,
	height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  title:{
    width: '45%',
    marginLeft: '5%'
  },
  checkMark:{
      width: '5%',
      marginRight: '7%'
  }
});
