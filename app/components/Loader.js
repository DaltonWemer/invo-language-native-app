import React from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity, 
  ScrollView,
  Dimensions,
  Image,
  TextInput
} from 'react-native'

// Class Wide Variables
const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const load = require('../../assets/loading.gif')

export default class Loader extends React.Component {
  render() {
    return (
        <View
            style={styles.container}
        >
            <Image
                style={styles.image}
                source={load}
            />
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        height: '100%',
        width: '100%',
        alignContent: 'center',
        justifyContent: 'center'
    },
    image: {
        alignSelf: 'center'
    },
});
