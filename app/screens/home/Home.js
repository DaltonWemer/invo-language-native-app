import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    SafeAreaView, 
    ScrollView,
    TouchableOpacity,
    AsyncStorage
} from 'react-native';

// Third Party
import axios from 'axios';
import Modal from 'react-native-modal';

// Custom Components
import { URL } from '../../config';
import { LanguageItem, Loader } from '../../components';

export default class Home extends React.Component {
    state = {
        languages: {},
        ready: false,
        selected: {},
        isModalVisible: false,
    }

    getData(){
        axios.get( 
         URL + 'api/languages',
       ).then((response) => {
         this.setState({
           languages: response.data.data,
           ready: true
         })
       }).catch((error) => {
         console.log(error)
       });
     }

     selectLanguage(language){
        this.setState({
            selected: language
        })
     }

     toggleModal(){
         this.setState({
            isModalVisible: !this.state.isModalVisible
         })
     }

     confirm(){
        AsyncStorage.setItem("language", this.state.selected.language_name)
        this.setState({savedLanguage: this.state.selected.language_name})
        this.toggleModal()
     }


     getSavedLanguage = async () => {
        const savedLanguage = await AsyncStorage.getItem('language');
            if(savedLanguage){
                this.setState({
                    savedLanguage: savedLanguage
                })
            }else {
                conosle.log('There is no saved language yet')
            }
      }

     componentDidMount(){
        this.getData()
        this.getSavedLanguage();
    }

    render(){
        const LanguageItems = () => {
            return this.state.languages.map(language => {
              return (
                <LanguageItem
                    key={language.id}
                    name={language.language_name}
                    code={language.language_code}
                    onPress={() => {this.selectLanguage(language)}}
                    selected={this.state.selected == language}
                />
              )
            }) 
          }

            return this.state.ready ? (
                <SafeAreaView style={styles.container}>
                    <Text
                        style={styles.title}
                    >
                        Select Your Language
                    </Text>
                    { this.state.savedLanguage &&
                        <Text
                            style={styles.subTitle}
                        >
                            Your Saved Language is {this.state.savedLanguage}
                        </Text>
                    }
                    <ScrollView
                        style={styles.scroll}
                    >
                        <LanguageItems/>
                    </ScrollView>
                    <TouchableOpacity
                        onPress={()=>{this.toggleModal()}}
                        style={styles.continue}
                    >
                        <Text
                            style={styles.confirmText}
                        >
                            Continue
                        </Text>
                    </TouchableOpacity>

                    <Modal
                        isVisible={this.state.isModalVisible}
                        onBackdropPress={()=>this.toggleModal()}
                    >
                        <View
                            style={styles.modal}
                        >
                            <Text
                                style={styles.modalTitle}
                            >
                                The language you have selected is {this.state.selected.language_name}
                            </Text>
                            <Text
                                style={styles.modalSubtitle}
                            >
                                Is this correct?
                            </Text>
                            <TouchableOpacity
                                onPress={()=>{this.confirm()}}
                                style={styles.confirm}
                            >
                                <Text
                                    style={styles.confirmText}
                                >
                                    Confirm
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={()=>{this.toggleModal()}}
                                style={styles.back}
                            >
                                <Text>Go Back</Text>
                            </TouchableOpacity>
                        </View>
                    </Modal>
                </SafeAreaView>
        ) : (
          <Loader/>
        )
}}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  title:{
      fontSize: 24
  },
  subTitle:{
    fontSize: 19
},
  scroll:{
    width: '95%',
    padding: 25,
  },
  continue:{
    backgroundColor: '#4293F5',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
    width: '80%',
    borderRadius: 10,
  },  
  confirmText:{
    color: '#FFF'
  },
  modal: {
    height: '25%',
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  modalTitle:{
    fontSize: 17,
    marginTop: '10%'
  },
  modalSubtitle:{
    fontSize: 15,
    marginTop: '5%',
    marginBottom: '7%'
  },
  confirm:{
    backgroundColor: '#4293F5',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    width: '80%',
    borderRadius: 10,
  },     
  back:{
      marginTop: '5%',
  },
  backText:{
    color: '#4293F5'
  }
});
